'''
Tarefa 19/05
Passar informações dos países utilizando o return, o break, e continue
Utilizar informações de idioma, países, continente e população
'''
def instrução_return ():
    novo_país= input ("Digite um novo país: ")
    países=["Alemanha", "Japão", "China", "Espanha", "Canadá", "França", "Austrália", "Nigéria", "Rússia", "Itália"]
    idiomas=["alemão", "japonês", "chinês", "espanhol", "inglês", "francês", "inglês", "inglês", "russo", "italiano"]
    população=["83 milhões", "126 milhões", "1,4 bilhão","47 milhões", "38 milhões", "68 milhões", "26 milhões", "206 milhões", "144 milhões", "59 milhões"]
    continente=["Europa", "Ásia", "Ásia", "Europa", "América", "Europa", "Oceania", "África", "Ásia", "Europa"]
    for país in países:
        if novo_país in países:
            indice=países.index(novo_país)
            print (f"{novo_país}, {idiomas[indice]}, {população[indice]}, {continente[indice]}")
        elif not novo_país in países:
            países.append (novo_país)
            print (f"{novo_país} não está na lista.")
        return (países)


def instrução_break ():
    '''
    Utilizado apenas no loop_for
    Interrompe o loop_for e vai para o próximo bloco de código
    '''
    países=instrução_return ()
    for país in países:
        if país == "Alemanha":
            print ("Este país está na lista.")
            break
        elif país == "Japão":
            print ("Este país está na lista.")
            break
        elif país =="China":
            print ("Este país está na lista.")
            break
        elif país =="Espanha":
            print ("Este país está na lista.")
            break
        elif país=="Canadá":
            print ("Este país está na lista.")
            break
        elif país=="França":
            print ("Este país está na lista.")
            break
        elif país=="Austrália":
            print ("Este país está na lista.")
            break
        elif país=="Nigéria":
            print ("Este país está na lista.")
            break
        elif país =="Rússia":
            print ("Este país está na lista.")
            break
        elif país=="Itália":
            print ("Este país está na lista.")
            break
        else:
            print (f"Este país {país} não está na lista.")
    print ("Saiu do loop_for.")


def instrução_continue ():
   países=instrução_return ()
   for país in países:
        if país=="Alemanha":
            continue
            print("Este país está na lista.")
        elif país =="Japão":
            continue
            print ("Este país está na lista.")
        elif país =="China":
            continue
            print ("Este país está na lista.")
        elif país =="Espanha":
            continue
            print ("Este país está na lista.")
        elif país =="Canadá":
            continue
            print ("Este país está na lista.")
        elif país== "França":
            continue
            print ("Este país está na lista.")
        elif país=="Austrália":
            continue
            print ("Este país está na lista.")
        elif país =="Nigéria":
            continue
            print ("Este país está na lista.")
        elif país =="Rússia":
            continue
            print ("Este país está na lista.")
        elif país =="Itália":
            continue
            print ("Este país está na lista.")      
        if país=="Brasil":
            print("Este país não está na lista.")
   print("Saiu do loop_for.")



def main ():
    parametro_return=instrução_return ()
    print (parametro_return)
    instrução_break ()
    instrução_continue ()

if __name__ == "__main__":
    main ()