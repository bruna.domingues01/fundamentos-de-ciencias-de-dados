import requests
from bs4 import BeautifulSoup
from bpc_teste import bpc_pdf
from banco_json import inserir_db

def acessar_pagina(url):
    pagina=requests.get(url)
    bs=BeautifulSoup (pagina.text, "html.parser")
    return bs

def paginas():
    paginas = []
    contador = 7
    while contador >=0:
        if contador != 0:
            url = "https://bricspolicycenter.org/publicacoes-pt/radar/page/"
            url = url + str(contador)
            contador = contador - 1
            paginas.append(url)
        elif contador == 0:
            url = "https://bricspolicycenter.org/publicacoes-pt/radar/" 
            contador = contador -1
            paginas.append(url)
    print (paginas)
    return paginas

def extrair_info ():
    for index, pagina in enumerate(paginas(), start=1):
        print(index, pagina)
        bs=acessar_pagina(pagina)
        lista_de_tags_artigos=bs.find_all ("article") 
        for tag_articule in lista_de_tags_artigos:
            titulo=tag_articule.a["title"]
            link=tag_articule.a["href"]
            page=acessar_pagina(link)
            tag_p=page.find("div", attrs={"class": "post_content"}).find_all("p")[:-1]
            link_pdf=bpc_pdf(link)
            print (titulo)      
            print (link)
            print (tag_p)
            print (link_pdf)
            print ("#########")
            inserir_db (titulo,link,data_normalizada,tag,paragrafos,nome)


 

def main ():
    extrair_info ()

if __name__ == "__main__":
    main ()