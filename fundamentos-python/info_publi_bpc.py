import requests
from bs4 import BeautifulSoup
from bpc_teste import bpc_pdf
from banco_json import inserir_db
import shutil 
import os
from time import sleep

def acessar_pagina(url):
    pagina=requests.get(url)
    bs=BeautifulSoup (pagina.text, "html.parser")
    return bs

def paginas():
    paginas = []
    contador = 9
    while contador >=0:
        if contador != 0:
            url = "https://bricspolicycenter.org/publicacoes-pt/bpc-policy-brief/page/"
            url = url + str(contador)
            contador = contador - 1
            paginas.append(url)
        elif contador == 0:
            url = "https://bricspolicycenter.org/publicacoes-pt/bpc-policy-brief/" 
            contador = contador -1
            paginas.append(url)
    print (paginas)
    return paginas


def extrair_info ():
    for index, pagina in enumerate(paginas(), start=1):
        print(index, pagina)
        bs=acessar_pagina(pagina)
        lista_de_tags_artigos=bs.find_all("article") #find all retorna uma lista
        for tag_articule in lista_de_tags_artigos:
            titulo=tag_articule.a["title"]
            link=tag_articule.a["href"]
            page=acessar_pagina(link)
            tag_p=page.find("div", attrs={"class": "post_content"}).find_all("p")[:-1]
            try:
                link_pdf=bpc_pdf(link)
            except:
                link_pdf = "NA"
            sleep (10)
            origem="/home/lantri_brunadomingues/Downloads"
            arquivos=os.listdir(origem)
            print(arquivos)
            if arquivos:
                shutil.move(f"{origem}/{arquivos[0]}",f"/home/lantri_brunadomingues/codigo/fundamentos-de-ciencias-de-dados/fundamentos-python/pdf")
            data_normalizada="NA"
            tag="NA"
            categoria="NA"
            paragrafos=tag_p
            print (f'TITULO: {titulo}')
            print (f'LINK: {link}')
            print (f'TAG-P: {tag_p}')
            print (f'LINK_PDF_2: {link_pdf}')
            print ("#########")
            site="bpc_policy_brief"
            inserir_db (site=site, titulo=titulo,link=link,data_normalizada=data_normalizada,tag=tag,paragrafos=paragrafos,categoria=categoria)



def main ():
    extrair_info()

if __name__ == "__main__":
    main ()