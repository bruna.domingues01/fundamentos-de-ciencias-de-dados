import requests
from bs4 import BeautifulSoup

def acessar_pagina(url):
    pagina=requests.get(url)
    bs=BeautifulSoup (pagina.text, "html.parser")
    return bs

def paginas():
    paginas = []
    contador = 6
    while contador >=0:
        if contador != 0:
            url = "https://bricspolicycenter.org/publicacoes-pt/fact-sheet/page/"
            url = url + str(contador)
            contador = contador - 1
            paginas.append(url)
        elif contador == 0:
            url = "https://bricspolicycenter.org/publicacoes-pt/fact-sheet/" 
            contador = contador -1
            paginas.append(url)
    print (paginas)
    return paginas

def extrair_info ():
    for index, pagina in enumerate(paginas(), start=1):
        print(index, pagina)
        bs=acessar_pagina(pagina)
        lista_de_tags_artigos=bs.find_all ("article") 
        for tag_articule in lista_de_tags_artigos:
            titulo=tag_articule.a["title"]
            link=tag_articule.a["href"]
            page=acessar_pagina(link)
            try: 
                tag_p=page.find("div", attrs={"class": "post_content"}).find_all("p")
                paragrafos=[]
                for paragrafo in tag_p:
                    paragrafos.append (paragrafo.text)
            except:
                pass
            print (titulo)
            print (link)
            print (paragrafos)
            print ("#########")


def main ():
    extrair_info ()

if __name__ == "__main__":
    main ()