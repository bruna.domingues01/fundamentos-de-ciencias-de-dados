import requests
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from bs4 import BeautifulSoup
from time import sleep
import lxml


def bpc_pdf(link):
    navegador = webdriver.Firefox()
    navegador.get(link)
    sleep(10)
    download_pdf=navegador.find_element(By.CSS_SELECTOR,".qbutton")
    download_pdf.click()
    pop_up_01=navegador.find_element(By.CSS_SELECTOR, "span.wpcf7-form-control-wrap:nth-child(3) > span:nth-child(1) > span:nth-child(4) > input:nth-child(1)")
    pop_up_01.click()
    pop_up_02=navegador.find_element(By.CSS_SELECTOR, "span.wpcf7-form-control-wrap:nth-child(7) > span:nth-child(1) > span:nth-child(2) > input:nth-child(1)")
    pop_up_02.click()
    pop_up_03=navegador.find_element(By.CSS_SELECTOR,".has-spinner")
    pop_up_03.click()
    link_pdf=navegador.current_url
    print (f'LINK PDF: {link_pdf}')
    sleep(25)
    navegador.close()
    return(link_pdf)

def main ():
   bpc_pdf()



if __name__ == "__main__":
    main ()