def loop_for():
    países=["Brasil", "Argentina", "África do Sul", "Portugal"]
    idiomas=["português", "espanhol", "inglês", "português"]
    #print(países, type (países))
    for país, idioma in zip (países, idiomas):
        #print(país)
        print(f"O nome do país é: {país}")
        print(f"O idioma do país é: {idioma}")
    teste=[país.upper() for país in idiomas]
    print (teste)
    novos_países=["Alemanha", "Egito", "Japão"]
    for país in novos_países:
        caixa_alta=país.upper()
        países.append(caixa_alta)
    print (países)

def tarefa_loop_for ():
    tarefa_países= ["Alemanha", "Japão", "China", "Espanha", "Canadá", "França", "Austrália", "Nigéria", "Rússia", "Itália"]
    tarefa_idiomas= ["alemão", "japonês", "chinês", "espanhol", "inglês", "francês", "inglês", "inglês", "russo", "italiano"]
    tarefa_população= ["83 milhões", "126 milhões", "1,4 bilhão","47 milhões", "38 milhões", "68 milhões", "26 milhões", "206 milhões", "144 milhões", "59 milhões"]
    tarefa_continente= ["Europa", "Ásia", "Ásia", "Europa", "América", "Europa", "Oceania", "África", "Ásia", "Europa"]
    for país, idioma, população, continente in zip (tarefa_países, tarefa_idiomas, tarefa_população, tarefa_continente):
        print (f"O nome do país é: {país}")
        print (f"O idioma do país é: {idioma}")
        print (f"A população do país é: {população}")
        print (f"O continente do país é: {continente}")
        print ("#####")

def tarefa_range_enumerate ():
    tarefa_países= ["Alemanha", "Japão", "China", "Espanha", "Canadá", "França", "Austrália", "Nigéria", "Rússia", "Itália"]
    for index, país in enumerate (tarefa_países, start=1):
        print (index, país)
   
def range_enumerate ():
    novos_países=["Alemanha", "Egito", "Japão"]
    for i in range(20,50,5):
        print (i)
    for index,país in enumerate(novos_países, start=1):
        print (index,país)

def loop_while ():
    print ("loop_while")
    a=10
    while a>0:
        print(a)
        a-=1
        #a=a-1 ou a-=1 são equivalentes

def exemplo_loop_while ():
    contador=240
    while contador>0:
        url="https://www.gov.br/mre/pt-br/canais_atendimento/imprensa/notas-a-imprensa?b_start:int="
        url= url+str(contador)
        contador= contador-30
        print (url)

def main ():
   #loop_for()
   #loop_while()
   #exemplo_loop_while()
    #range_enumerate()
    #print ("teste")
    tarefa_loop_for ()
    tarefa_range_enumerate ()


if __name__ == "__main__":
    main ()

