import requests
from bs4 import BeautifulSoup
import urllib.parse
from banco_json import inserir_db


def acessar_pagina(url):
    pagina=requests.get(url)
    bs=BeautifulSoup (pagina.text, "html.parser")
    return bs

def paginas():
    paginas = []
    contador = 5 #191
    while contador >=0:
        if contador != 0:
            url = "https://www.cepal.org/pt-br/news/list/language/es?page="
            url = url + str(contador)
            contador = contador - 1
            paginas.append(url)
        elif contador == 0:
            url = "https://www.cepal.org/pt-br/news/list/language/es" 
            contador = contador -1
            paginas.append(url)
    print (paginas)
    return paginas

def coleta_cepal_noticias ():
    cepal="https://www.cepal.org/"
    cepal_paginas=["https://www.cepal.org/pt-br/news/list/language/es?page=1"]
    for index, pagina in enumerate(cepal_paginas, start=1):
        print(index, pagina)
        bs=acessar_pagina(pagina)
        titulos_links=bs.find("div",attrs={"class":"panel-pane pane-views-row"}).find_all("h3")
        horario="NA"
        datas=bs.find("div",attrs={"class":"panel-pane pane-views-row"}).find_all("span", attrs={"class": "date-display-single"})
        categoria="NA"
        tags=bs.find("div",attrs={"class":"panel-pane pane-views-row"}).find_all ("span", attrs={"class": "views-field-type"})
        for titulo_link, data, tag in zip (titulos_links, datas, tags):
            titulo=titulo_link.text
            link=urllib.parse.urljoin (cepal,titulo_link.a["href"])
            data=data.text
            tag=tag.text
            page=acessar_pagina(link)
            try: 
                tag_p=page.find("div", attrs={"class": "field field-name-field-body field-type-text-long field-label-hidden"}).find_all("p")
                paragrafos=[]
                for paragrafo in tag_p:
                    paragrafos.append (paragrafo.text)
            except:
                pass
            #tag_ul=page.find("div", attrs={"class": "field-item even"}).find_all("li")
            #tags=[]
            #for tag in tag_ul:
                #tags.append (tag.text)
            print (titulo)
            print (link)
            print (data)
            print (horario)
            print (tag)
            print (paragrafos)
            print (categoria)
            #print (tags)
            print ("####")
            data_normalizada=ajustar_data(data)
            print(data_normalizada)
            inserir_db (titulo,link,data_normalizada,horario,tag,paragrafos,categoria)

def ajustar_data(data):
    meses={
        "janeiro":"01", 
        "fevereiro":"02", 
        "março":"03", 
        "abril":"04", 
        "maio":"05", 
        "junho":"06", 
        "julho":"07", 
        "agosto":"08", 
        "setembro":"09",
        "outubro":"10",
        "novembro":"11",
        "dezembro":"12"
        }
    data_lista=data.split()
    print(data_lista[2]) 
    mes=meses[data_lista[2]]
    print(mes)
    data_normalizada=f"{data_lista[0]}/{mes}/{data_lista[-1]}"
    return(data_normalizada)

def main ():
   coleta_cepal_noticias ()
   #ajustar_data()


if __name__ == "__main__":
    main ()