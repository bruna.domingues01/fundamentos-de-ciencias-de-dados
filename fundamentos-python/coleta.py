import requests
from bs4 import BeautifulSoup

def acessar_pagina(url):
    pagina=requests.get(url)
    bs=BeautifulSoup (pagina.text, "html.parser")
    return bs


def coleta_bcp_policy_brief ():
    """coletar autor, link, pdf, titulo e resumo"""
    url="https://bricspolicycenter.org/publicacoes-pt/bpc-policy-brief/"
    bs=acessar_pagina(url)
    lista_de_tags_artigos=bs.find_all ("article") #find all retorna uma lista
    for tag_articule in lista_de_tags_artigos:
        titulo=tag_articule.a["title"]
        link=tag_articule.a["href"]
        print (titulo)
        print (link)
        print ("#########")

def coleta_bpc_paper ():
    url="https://bricspolicycenter.org/publicacoes-pt/bpc-paper/"
    bs=acessar_pagina(url)
    lista_de_tags_artigos=bs.find_all ("article") 
    for tag_articule in lista_de_tags_artigos:
        titulo=tag_articule.a["title"]
        link=tag_articule.a["href"]
        print (titulo)
        print (link)
        print ("#########")

def coleta_fact_sheet ():
    url="https://bricspolicycenter.org/publicacoes-pt/fact-sheet/"
    bs=acessar_pagina(url)
    lista_de_tags_artigos=bs.find_all ("article") 
    for tag_articule in lista_de_tags_artigos:
        titulo=tag_articule.a["title"]
        link=tag_articule.a["href"]
        print (titulo)
        print (link)
        print ("#########")

def coleta_radar ():
    url="https://bricspolicycenter.org/publicacoes-pt/radar/"
    bs=acessar_pagina(url)
    lista_de_tags_artigos=bs.find_all ("article") 
    for tag_articule in lista_de_tags_artigos:
        titulo=tag_articule.a["title"]
        link=tag_articule.a["href"]
        print (titulo)
        print (link)
        print ("#########")

def coleta_noticias_bpc ():
    url="https://bricspolicycenter.org/noticias/"
    bs=acessar_pagina(url)
    lista_de_tags_artigos=bs.find_all ("article") 
    for tag_articule in lista_de_tags_artigos:
        titulo=tag_articule.a["title"]
        link=tag_articule.a["href"]
        print (titulo)
        print (link)
        print ("#########")

def coleta_cepal_noticias ():
    url="https://www.cepal.org/pt-br/news/list/language/es"
    bs=acessar_pagina(url)
    lista_tag_h2=bs.find ("div", {"class": "painel-pane pane-views-row"}).find_all("h2")
    for tag_h2 in lista_tag_h2:
        titulo=tag_h2.text
        link=tag_h2.a["href"]
        print (titulo)
        print (link)
        print ("#####")


def main ():
    coleta_bcp_policy_brief()
    #coleta_bpc_paper ()
    #coleta_fact_sheet ()
    #coleta_radar ()
    #coleta_noticias_bpc ()
    #coleta_cepal_noticias ()


if __name__ == "__main__":
    main ()