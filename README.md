# Fundamentos de Ciências de Dados

## Comandos para versionamento (git)

Digitar os comandos abaixo na raiz do projeto para versionar o código e subí-lo no gitlab

```
git add .
git commit -m "insira mensagem aqui"
git pull origin main
git push origin main
```

## Criação do Ambiente Virtual (conda)

```
conda activate env_bruna
conda env update

```

## Comandos básicos do terminal Linux

```
ls
cd
mkdir
history
cat
```

## HTML/CCS/JS
```
HTML>> responsável pela estrutura da paǵina web (prédio: estrutura) (linguagem de marcação)
CSS>> responsável pela formatação da página web (prédio: acabamento/pintura/etc) (lingugagem de estilo)
JS>> responsável pelas interações da página web (java script) (liguagem de programação)

-possibilidade de coleta a partir: das tags da html; do css selector; do xpath
```


Tarefa para 05/05
Acrescentar 10 países, colocar idiomas, população e continente 
"1 - Informações sobre Brasil: português, 200 milhões, América"

Tarefa para 12/05
-Passar o input com o nome de um país, se o país for satisfeito, printar as informações o país, população, idioma e continente; caso não for satisfeito apontar, com else, que não consta nos países selecionados 
-Passar a lista de países já existente no return, quando passar na main, retornar a lista, e acrescentar novos países
-Trazer o tema de pesquisa, e a indicação de três fontes -algum site- de dados importantes paras pesquisas pessoais

Tarefa para 19/05
-Passar informações dos países utilizando o return, o break, e continue
-Utilizar informações de idioma, países, continente e população
-Trazer o tema de pesquisa, e a indicação de três fontes -algum site- de dados importantes paras pesquisas pessoais

Tarefa 26/05
-Nos sites escolhidos, replicar o que foi feito na reunião -a estrutura-

